import Head from 'next/head'

function Layout ({ children, title }) {
  return (
    <div>
      <Head>
        <title>{title ? `${title} | ` : ''}ponway.life</title>
      </Head>

      <h1>ponway.life</h1>

      {children}

      <style jsx global>{`
      `}</style>
    </div>
  )
}

export default Layout
