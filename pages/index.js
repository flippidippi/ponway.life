import Layout from '~/components/Layout'
import IndexMd from '~/markdown/index.md'

function Index () {
  return (
    <Layout>
      <IndexMd />
    </Layout>
  )
}

export default Index
