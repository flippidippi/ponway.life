import Youtube from '~/components/Youtube'

## Rules
Ponway is a game that shares most rules with racquetball, with a few notable exceptions.
Instead of the standard ball, a larger inflatable ball is used.
Racquets are not used and the ball can be hit with any part of the body.

<Youtube id='jXrqs1xFkW0' />

## Requirements
### Equipment
- Ponway ball - [Hedstrom Playball](http://www.hedstrom.com/products/balls-2/hedstrom-playballs)
  - These can be found at department stores like Target and Walmart and sports stores like Academy.
  - You want to fill the ball so that it's slightly firm and bounces well, but don't over inflate as it will hurt when hitting and shorten the life of the ball.

### Players
Ponway can be played with 2-4 players depending on game type.

- 2v2 (classic ponway) - 4 players
- 1v1 - 2 players
- 1v1v1 - 3 players
- Cut Throat - 3 players
- Last Hits - 2-4 players

### Court
The court is a traditional racquetball court, but the only lines used are the ones that contain the service zone.

![ponway court](/static/img/ponway-court.png)

## Rules
The rules will be defined as a game of classic 2v2 ponway.
Rules for other games types can be found under Additional Game Type Rules.

### Serving
Play begins with the server and teammate standing in the service zone and serving the ball to their opponent.
Players on a team must alternate who serves on successive serves.
The receiving team must stand behind the back line of the service zone as to not obstruct the serve.

The serve must first hit the front wall and then pass the second line red line, before hitting the ground, else it is a fault.
The server may hit the ball with any part of their body.
The server may choose to bounce the ball once before hitting it, up it to themselves, simply drop it, or lay it on the ground.
In its flight, the ball may strike side walls or the ceiling.
The ball is in play after making contact with the front wall and then passing into the rear half of the court.

#### Faults
The server is given two opportunities to put the ball into play.
If the server hits two faults in a row, the opposing team takes over in the service zone and the next server on that team serves.
If a mistake is made but the ball does not get hit, it is not a fault, since the ball was not yet in play, but the serve simply needs to be redone.
A fault can happen in multiple ways.

- A serve that hits the ground before carrying over back line of the service zone.
- A serve that hits the back wall without bouncing on the ground first.
- A serve that hits a member of the serving team before the opposing team is able to respond.
- Either players on the serving team stepping outside the service zone while the ball is being served.

### Volleys
A volley begins after a successful serve.
To return serve, the receiving team must stand anywhere behind the back line of the service zone.
Each team member may touch the ball once in order to return the ball to the front.
Since each player can hit the ball once on the return, a player may up the ball to their teammate, like in volleyball.
They need to return the serve before it bounces a second time.
The ball must travel to the front wall for it to be a good return of serve.
The ball may hit any surface except the floor on the return as long as it hits the front wall before bouncing.
If a player strikes the ball before it hits the ground, it must be returned to the front wall without bouncing on the ground.

Once the ball is in play, each player alternates hitting the ball until one misses the ball or hits an illegal shot.
A player can strike the ball with any part of your body but may not catch or throw the ball.
Players try to earn points or win the serve by putting an end to a volley.
Often this is done when a player's shot hits the front wall at its lowest point, causing the ball to roll out, rather than bounce back into the playing area (called a krit).
Points are also earned when rallies end with an error, i.e. when the ball makes contact with the floor before reaching the front wall.

Once the ball is in play, the walls and ceiling can be used for shot variations.
Points are scored when after serving the ball, the serving team wins the volley.
If the returning team wins the volley, the result is a sideout, no points are scored for either team and the team who won the volley gets to serve.
Whoever wins the volley always serves next.

### Obstructions
Obstructions are stoppages of play, and result in the replay of the point or an automatic point awarded.
When determining obstructions or valid hits, use your best judgement.
If a consensus cannot be reached, replay the point.
It is your responsibility to give your opponent enough room to hit the shot.
Obstructions are:

- If a ball striking any part of the court that results in an erratic rebound (fan vents, door knob, lights, holes in the wall, GoPros, etc), replay the point.
- If an opposing player unintentionally contacts opponent while they are attempting to make a play on the ball, replay the point.
- If an opposing player unintentionally screens the opponent's view or ability to get to the ball, replay the point.
- If a player is unable to reach the ball because their opponent is in the way, replay the point.
- If an intentional obstruction of the ball on the return to the wall, obstructed team wins the volley.
- If the ball hits a player that is not on the currently returning team, obstructed team wins the volley.
- If an opposing player has a reasonable distance from the returning team and is hit by the ball, the volley is still alive for the returning team and resets the hit count for the returning team.
  - If after the opposing player is hit and the returning team wins the volley of the hit, that team gets an additional point regardless of whether they were the serving team or not.

### Scoring
A game continues until one team reaches the score of 15.
However, the team must win by two or more points, and the opposing team must successfully return the ball from the serve to score the winning point.
If the returning team fails to return the serve, the serving team still receives a point but does not win the game.
If a team reaches 21 points, they automatically win.

## Additional Rules
There are a few additional rules that add flair to the game of Ponway.
Some of these rules are optional and are therefore marked with a <i className='fi-wrench'></i>.

### Fatality
Sometimes the Ponway ball goes straight up in the air because physics.
When this happens unintentionally, the player who hit it up there may declare the phrase “Finish Him/Her!” to get an additional hit on the ball.
If the ball is then successfully returned and the rally finished, the player has performed a Fatality, and gets an additional point
This means that if you are the server, you get 2 points, and if you are the returning player, you get 1 point and the next serve.
This is cannot be used in 2v2.

### Mike Ross
Sometimes the ponway ball hits people instead of a wall or the ceiling.
If the ball hits a player's backside and that player is not intentionally in the way of the ball, the ball is still considered in play.
Therefore, if the ball then falls to the ground, it is the error of the player who intended to hit the back wall.
But, if the ball hits a player and continues to the back wall, that player is still responsible for the return as if the ball never hit them.

### Mike Ross Fatality <i className='fi-wrench'></i>
Rarely, when someone gets Mike Ross-ed, the ball will go straight up in the air.
The returning player may then attempt a Fatality, and if the Fatality results in the rally ending, that player then automatically wins the game.

## Additional Game Type Rules
### 1v1

### 1v1v1

### Cuthroat
- The server always acts alone.
- The two non-server players are temporarily on the same team against the server. They are allowed to help each other in any way they would be allowed to in a 2v2 game.
- Players switch being servers in a predetermined order and always follow that order.

### Last Hits

## Terminology
- Krit -
- Magic -
- Moonman -
- Mike Ross -
