const path = require('path')
const withMDX = require('@zeit/next-mdx')({
  extension: /\.mdx?$/
})

module.exports = withMDX({
  pageExtensions: ['js', 'jsx', 'mdx'],
  webpack: function (config, { dev, isServer }) {
    // Setup aliases
    config.resolve.alias = {
      ...config.resolve.alias,
      '~': path.resolve(__dirname)
    }

    return config
  }
})
